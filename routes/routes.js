const router = require('express').Router();


router.get('/',(req,res)=>{
    res.render('signin');
});
router.get('/signup',(req,res)=>{
    res.render('signup');

});
router.get('/verify',(req,res)=>{
    res.render('verify');
    
});


router.get('/forgotpassword',(req,res)=>{
    res.render('reset_pass_mail');
});
router.get('/store_setup',(req,res)=>{
    res.render('store_initialise');
});

router.get('/product',(req,res)=>{
    res.render('store_setup_product');
});
router.get('/subscription',(req,res)=>{
    res.render('store_setup_subscription');
});

router.get('/plan',(req,res)=>{
    res.render('store_setup_plan');
});

router.get('/payment',(req,res)=>{
    res.render('store_setup_payment');
});

router.get('/changepwd',(req,res)=>{
    var email=req.query.email;
    //console.log("email checking in change password:" + email)
    res.render('choose_password',{email:email});
});

module.exports = router;